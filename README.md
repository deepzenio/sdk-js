# DeepZen JS SDK

This repository will contain a set of JS libraries for DeepZen services. To get more details of DeepZen API, check out [our API documentation.](https://developers.deepzen.io)

## Streaming

When using [Create Session](https://developers.deepzen.io/#1128f948-b84e-0e14-0b0c-0c0b5c0e8b75) service, API returns a `Job ID` that you can use to track the status of your synthesis. Using **Streaming** SDK, you can start playing a synthesis before it is completed. You can do this by passing the `Job ID` returned from API.

```
new Deepzen.Streamer("JOB ID", {
	onReady() {},
	onPlay() {},
	onPause() {},
	onProgress(completedSeconds, totalSeconds) {}
});
```

Using the callbacks you can construct your own Audio Player UI.

### Methods

- play()
- pause()
- seekTo(percentage)
- destroy()

### Example

An [example](examples/vanilla.html) can be found in examples directory.
